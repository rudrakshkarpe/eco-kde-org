---
title: Get Involved
name: KDE Eco
userbase: KDE Eco
menu:
  main:
    weight: 8
---
## Become a part of the sustainable software movement

We need as many motivated people as possible to drive this forward. Here are some channels where you can get more information and contribute.

### Community & Support

- BigBlueButton: Monthly meet-ups, 2nd Wednesdays 19:00 CEST/CET (Berlin time) (contact us for details)
- Matrix room: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Energy Efficiency Mailing list: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- KDE Eco Forum: https://forum.kde.org/viewforum.php?f=334

### Resources

- FEEP GitLab repository: https://invent.kde.org/teams/eco/feep
- BE4FOSS GitLab repository: https://invent.kde.org/teams/eco/be4foss
- Blue Angel Applications: https://invent.kde.org/teams/eco/blue-angel-application

### Contact

Email: `joseph [at] kde.org`
